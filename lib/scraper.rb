require 'HTTParty'
require 'Nokogiri'
require 'Pry'

# Scrapes FOTMOBs club specific news page, parses the doc, formats the data, and returns a formatted hash.
class Scraper
  BASE_URL = 'https://www.fotmob.com/teams'.freeze

  def initialize(club, content)
    doc = HTTParty.get("#{BASE_URL}/#{club[:id]}/#{content}/#{club[:url_name]}")
    @content = content
    @page = Nokogiri::HTML(doc)
  end

  # DRYs up code by dynamically calling class based on the content type passed in.
  # See README for more detail.
  def send
    Kernel.const_get("Pages::#{@content.capitalize}").new(page: @page).collection
  end
end
