module PremierLeague
  CLUBS = {
    arsenal: {
      id: '9825',
      url_name: 'arsenal'
    },
    brighton: {
      id: '10204',
      url_name: 'brighton-&-hove-albion'
    },
    burnley: {
      id: '8191',
      url_name: 'burnley'
    },
    chelsea: {
      id: '8455',
      url_name: 'chelsea'
    },
    everton: {
      id: '8668',
      url_name: 'everton'
    },
    fulham: {
      id: '9879',
      url_name: 'fulham'
    },
    leeds: {
      id: '8463',
      url_name: 'leeds-united'
    },
    leicester: {
      id: '8197',
      url_name: 'leicester-city'
    },
    liverpool: {
      id: '8650',
      url_name: 'liverpool'
    },
    mancity: {
      id: '8456',
      url_name: 'manchester-city'
    },
    manunited: {
      id: '10260',
      url_name: 'manchester-united'
    },
    newcastle: {
      id: '10261',
      url_name: 'newcastle-united'
    },
    palace: {
      id: '9826',
      url_name: 'crystal-palace'
    },
    sheffield: {
      id: '8657',
      url_name: 'sheffield-united'
    },
    southampton: {
      id: '8466',
      url_name: 'southampton'
    },
    spurs: {
      id: '8586',
      url_name: 'tottenham-hotspur'
    },
    villa: {
      id: '10252',
      url_name: 'aston-villa'
    },
    westbrom: {
      id: '8659',
      url_name: 'west-bromwich-albion'
    },
    westham: {
      id: '8654',
      url_name: 'west-ham-united'
    },
    wolves: {
      id: '8602',
      url_name: 'wolverhampton-wanderers'
    }
  }.freeze
end
