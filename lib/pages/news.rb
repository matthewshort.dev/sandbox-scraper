module Pages
  # Collection of articles for a given club (page)
  class News
    attr_reader :page

    def initialize(page:)
      @page = page
    end

    def collection
      news_container.map do |element|
        @article = element
        {
          href: href,
          img_src: img_src,
          title: title,
          source: source
        }
      end
    end

    private

    attr_reader :article

    def href
      article.attributes['href'].value
    end

    def img_src
      article.children
             .css('.ResizableImage')
             .children[0]
             .attributes['src']
             .value
    end

    def news_container
      data = @page.css('.RelatedNewsListItself')
                  .children.map { |element| element }
      data.shift

      data
    end

    def source
      article.css('.ArticleSource').children[1].text
    end

    def title
      article.children.css('.ArticleTitle').children.text
    end
  end
end
