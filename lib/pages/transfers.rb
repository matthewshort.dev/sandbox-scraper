module Pages
  # Recent transfer and player contract extensions for a given club
  class Transfers
    attr_reader :page, :transfers

    def initialize(page:)
      @page = page
      @transfers = []
    end

    def collection
      i = 0

      while i < names.length
        transfers.push(
          player: names[i],
          position: positions[i],
          fee: fees[i] || '-',
          deal_type: in_outs[i] || 'Extension',
          club: clubs[i]
        )
        i += 1
      end

      transfers
    end

    private

    def clubs
      page.css('.css-dad5eq-transferTextStyle')
          .children
          .map { |e| e }
          .map { |e| e.text unless e.name.eql?('span') || e.text.eql?('') }
          .compact
    end

    def fees
      page.css('.css-1543qq4-feeText').children.map(&:text)
    end

    def in_outs
      page.css('.css-dad5eq-transferTextStyle')
          .children
          .map { |e| e }
          .map { |e| e.text if e.name.eql?('span') }
          .compact
    end

    def names
      page.css('.css-8i3m6g-playerName').children.map(&:text)
    end

    def positions
      page.css('.css-141ugcr-playerPosition').children.map(&:text)
    end
  end
end
