require './lib/scraper.rb'
require './lib/pages/news.rb'
require './lib/pages/transfers.rb'
require './lib/helpers/clubs/premier_league.rb'

################################################

club = PremierLeague::CLUBS[:liverpool]
content = {
  news: 'news',
  squad: 'squad',
  fixtures: 'fixtures',
  transfers: 'transfers'
}

################################################

transfers = Scraper.new(club, content[:transfers]).send
transfers.each do |transfer|
  puts '-------------------------------'
  puts "Deal Type: #{transfer[:deal_type]}"
  puts "Club: #{transfer[:club]}"
  puts "Player: #{transfer[:player]}"
  puts "Position:  #{transfer[:position]}"
  puts transfer[:fee].capitalize
  puts "-------------------------------\n\n"
end

################################################

news = Scraper.new(club, content[:news]).send
news.each do |article|
  puts '-------------------------------'
  puts "title: #{article[:title]}"
  puts "href:  #{article[:href]}"
  puts "source: #{article[:source]}"
  puts "img_src: #{article[:img_src]}"
  puts "-------------------------------\n\n"
end

################################################
