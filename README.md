# sandbox-scraper
This project exists as a proof of concept and testing grounds for a potential backend solution to sourcing news articles for football clubs in lfc-sandbox and euro-trip-io

## Requirements
* HTTParty => `gem install httparty`
* Nokogiri => `gem install nokogiri`
* Pry      => `gem install pry`

## Send Method
Using the `send` method in our `Scraper` object allows us to dynamically call a class based on the content that has been passed in.  In Rails, we would simply use `constantize`, but since this is a basic Ruby project we use the `Kernel` object.
```
Ex: @content = 'new'
 => Kernel.const_get("Pages::#{@content.capitalize}") == Pages::New
```